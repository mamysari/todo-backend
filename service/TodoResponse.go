package service

type SaveResponse struct {
	Status  int     `json:"status"`
	Data    TodoDTO `json:"data"`
	Message string `json:"message"`
}
type ListResponse struct {
	Status  int     `json:"status"`
	Message string `json:"message"`
	Data    []TodoDTO `json:"data"`
}

