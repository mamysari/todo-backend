package service

import (
	"encoding/json"
	"log"
	"net/http"
	"strings"
	"todo-backend/model"
	Constant "todo-backend/util"
)

var dbList []model.Todo

func List(w http.ResponseWriter, r *http.Request) {
	log.Println("Run List func")
	var dto []TodoDTO
	for _, item := range dbList {
		dto = append(dto, TodoDTO{
			Id:   item.Id,
			Todo: item.Todo,
		})
	}
	json.NewEncoder(w).Encode(ListResponse{
		Status: http.StatusOK,
		Data:   dto,
	})
}
func Save(w http.ResponseWriter, r *http.Request) {
	log.Println("Run Save func")
	var dto TodoDTO
	err := json.NewDecoder(r.Body).Decode(&dto)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(SaveResponse{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
		})
		return
	}
	dto.Todo = strings.Trim(dto.Todo, " ")
	if dto.Todo == "" {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(SaveResponse{
			Status:  http.StatusBadRequest,
			Message: Constant.ErrorMessage,
		})
		return
	}
	todo := model.Todo{
		Id:   len(dbList) + 1,
		Todo: dto.Todo,
	}
	dbList = append(dbList, todo)
	json.NewEncoder(w).Encode(SaveResponse{
		Status:  http.StatusOK,
		Message: Constant.SuccessfullyMessage,
		Data:    dto,
	})
}

func Status(w http.ResponseWriter, r *http.Request) {
	log.Printf("Status  Start")
}
