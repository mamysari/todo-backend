package service

type TodoDTO struct {
	Id   int    `json:"id"`
	Todo string `json:"value"`
}
