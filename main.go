package main

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"todo-backend/service"
)

func handleRequest() {
	router := mux.NewRouter()
	router.HandleFunc("/api/todos", service.List).Methods("GET")
	router.HandleFunc("/api/todos", service.Save).Methods("POST")
	router.HandleFunc("/api/status", service.Status).Methods("GET")
	log.Fatal(http.ListenAndServe(
		":8080",
		router))
}
func main() {
	log.Println("Service Starting")
	handleRequest()
}
