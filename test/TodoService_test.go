package TodoService_test

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
	"todo-backend/model"
	"todo-backend/service"
	Constant "todo-backend/util"
)

type Todos struct {
	Todos []model.Todo `json:"todos"`
}

var todos Todos

func TestReadFile(t *testing.T) {
	env := os.Getenv("backendTestEnv")
	jsonFile, err := os.Open(env)
	if err != nil {
		t.Fatal(err.Error(), env)
	}
	defer jsonFile.Close()
	byteValue, _ := ioutil.ReadAll(jsonFile)

	errUnmarshal := json.Unmarshal(byteValue, &todos)
	if errUnmarshal != nil {
		t.Fatal(errUnmarshal.Error())
	}
	if todos.Todos == nil {
		t.Fatal("Not Found Data")
	}
}
func TestSave(t *testing.T) {
	if todos.Todos == nil {
		t.Fatal("Not Found Data")
	}
	for _, todo := range todos.Todos {
		reqBody, _ := json.Marshal(todo)
		req, err := http.NewRequest("POST", "/todos", bytes.NewReader(reqBody))
		if err != nil {
			t.Fatal(err)
		}
		rr := httptest.NewRecorder()
		handler := http.HandlerFunc(service.Save)
		handler.ServeHTTP(rr, req)
		var saveRes service.SaveResponse
		errSaveRes := json.NewDecoder(rr.Body).Decode(&saveRes)
		if errSaveRes != nil {
			t.Error(errSaveRes.Error())
		}
		if strings.TrimSpace(todo.Todo) == "" {
			if status := rr.Code; status != http.StatusBadRequest {
				t.Errorf("handler returned wrong status code: got %v want %v",
					status, http.StatusBadRequest)
			}
			if status := saveRes.Status; status != http.StatusBadRequest {
				t.Errorf("handler returned wrong response status code: got %v want %v",
					status, http.StatusBadRequest)
			}
			if saveRes.Message != Constant.ErrorMessage {
				t.Errorf("handler returned wrong Error Message: got %v want %v.",
					saveRes.Message, Constant.ErrorMessage)
			}
		} else {
			if status := rr.Code; status != http.StatusOK {
				t.Errorf("handler returned wrong status code: got %v want %v . Request Body todo = %v",
					status, http.StatusOK, todo.Todo)
			}
			if status := saveRes.Status; status != http.StatusOK {
				t.Errorf("handler returned wrong response status code: got %v want %v",
					status, http.StatusOK)
			}
			if saveRes.Data.Todo != todo.Todo {
				t.Errorf("handler returned wrong response data : got %v want %v",
					saveRes.Data, todo.Todo)
			}
			if saveRes.Message != Constant.SuccessfullyMessage {
				t.Errorf("handler returned wrong Successfully Message: got %v want %v.",
					saveRes.Message, Constant.SuccessfullyMessage)
			}
		}
	}
}
func TestList(t *testing.T) {
	if todos.Todos == nil {
		t.Fatal("Not Found Data")
	}
	req, err := http.NewRequest("GET", "/todos", nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(service.List)
	handler.ServeHTTP(rr, req)
	var listRes service.ListResponse
	errListRes := json.NewDecoder(rr.Body).Decode(&listRes)
	if errListRes != nil {
		t.Error(errListRes.Error())
	}
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	if status := listRes.Status; status != http.StatusOK {
		t.Errorf("handler returned wrong response status code: got %v want %v",
			status, http.StatusOK)
	}
}
