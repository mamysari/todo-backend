FROM golang:1.12-alpine AS build_base

RUN apk add --no-cache git

WORKDIR /tmp/todo-backend
 
COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

RUN go build -o ./out/todo-backend .

FROM alpine:3.9

COPY --from=build_base /tmp/todo-backend/out/todo-backend /app/todo-backend

EXPOSE 8080

CMD ["/app/todo-backend"]